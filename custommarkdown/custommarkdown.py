import re
from mkdocs.plugins import BasePlugin

class CustomMarkdown(BasePlugin):
    config_scheme = ()

    # Will be trigered before markdown is interpretated
    def on_page_markdown(self, markdown, page, config, site_navigation=None, **kwargs):

        # First find the advanced box text and add a closing div
        # previous one :  ((?<=\+{3})(\n\s+(.*))+)
        advancedBox = re.findall(r"((?<=\+{3})(\n\s+(.*))+(!?\n))", markdown, flags=re.MULTILINE) 

        for element in advancedBox:
            markdown = markdown.replace(element[0], element[0] + '</div>')

        # Then replace +++ by a div tag with advancedBox class
        markdown = markdown.replace("+++", '<div class="advancedBox">')
        
        return markdown