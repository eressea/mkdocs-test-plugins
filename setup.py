from setuptools import setup, find_packages

setup(
    name='CustomMarkdown',
    version='1.0.0',
    url='',
    license='MIT',
    author='Solus-fr contributors',
    author_email='your@email.com',
    description='',
    install_requires=['mkdocs'],

    packages=find_packages(exclude=['*.tests']),

    # The following rows are important to register your plugin.
    # The format is "(plugin name) = (plugin folder):(class name)"
    # Without them, mkdocs will not be able to recognize it.
    entry_points={
        'mkdocs.plugins': [
            'custom-markdown = custommarkdown.custommarkdown:CustomMarkdown',
        ]
    },
)