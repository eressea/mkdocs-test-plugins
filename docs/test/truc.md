<input type='checkbox' id="test" class='switch-input'><label for="test" class='switch-label'>test</label>

# Welcome to MkDocs

For full documentation visit [mkdocs.org](https://mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

+++
    ceci est une superbe advanced box :D
    avec plusieurs lignes en plus !
+++
    En voici une autre pour tester le bon fonctionnement
<br>

juste pour etre sur 